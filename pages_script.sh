#!/bin/bash

#causes script to exit imediatly if error occurs
set -eu

#overrides all other localisation settings
LC_ALL=C

#creates directory cities
mkdir -p cities

#assignes varable DATE_FILE to current date 
DATE_FILE=$(date "+%F-%H-%M")

#creates directory using formated current date varable as dir name
mkdir -p cities/$DATE_FILE

#loops through every txt file in curerent date dir in scraped weather dir 
for CITY_X in scraped_weather/$DATE_FILE/*.txt
do	
		

	#fetches the capialized name of the city, assigns to variable
        CITY_NAME=$(cat "$CITY_X" | head -n1)

	#fetches and assigns a variable for each temperature in the txt file	
        TEMP=$(cat "$CITY_X" | head -n4 | tail -n3)
        TEMP_1=$(echo "$TEMP" | head -n1)
        TEMP_2=$(echo "$TEMP" | head -n2 | tail -n1)
        TEMP_3=$(echo "$TEMP" | head -n3 | tail -n1)
	
	#fetches preciptitaion
        PRE=$(cat "$CITY_X" | tail -n3 | head -n1)
	
	#fetches date of fetching
        DATE=$(cat "$CITY_X" | tail -n2 | head -n1)

	#fetches humidity
        HUM=$(cat "$CITY_X" | tail -n1)

	#assigns current date to variable
        DATE_0H=$(date "+%H:%M")
	
	#assigns date for next hour  
        DATE_1H=$(date "+%H:%M" -d '+1 hour')

	#assigns date for next two hours
        DATE_2H=$(date "+%H:%M" -d '+2 hours')
	
	#echoes every line in varable CITY_X, removes ".txt", assign to variable CITY_Y 
        CITY_Y=$(echo $CITY_X | sed -E s/\.txt$//g)

	#echos the html template with the variables displaying the forecast data
        echo "<!DOCTYPE html>
	<html>
        <head>
        <meta charset="UTF-8">
        <title> Forcast - $CITY_NAME</title>
	</head>
        <body>
        <h1> Forcast for $CITY_NAME </h1>
        <p>Temperatures:</p>
        <ul>
        <li>$DATE_0H: $TEMP_1 ℃</li><br>
        <li>$DATE_1H: $TEMP_2 ℃</li><br>
        <li>$DATE_2H: $TEMP_3 ℃</li><br>
        </ul>
        <p>Forcast: $PRE</p>
        <p>Humidity: $HUM%</p>
        <p>Fetched: $DATE</p><br> 
        <p><a href="../../index.html">Weather forcast for other Norwegian cities.</a></p>
        </body>  
	 </html>" > $CITY_Y.html

	#moves html files to the cities/DATE_FILE directory 
        mv $CITY_Y.html ./cities/$DATE_FILE
	
done



