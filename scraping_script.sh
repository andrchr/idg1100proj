#!/bin/bash

#causes script to exit imediatly if error occurs
set -eu

#overrides all other localisation settings
LC_ALL=C

#Creates directory scraped_weather
mkdir -p scraped_weather

#assigning city names to variable NAMES
NAMES="bergen oslo gjovik trondheim tromsoe kristiansand"

#assigning filename containing current date in the variable DATE_FILE
DATE_FILE=$(date "+%F-%H-%M")

#assigning DATE variable to be used as date within the txt file
DATE=$(date "+%F %H:%M")

#assinging varibale DAY to display what nr day in the month, 1 - ~31, to be used for determaning odd/even day for Kristiansand
DAY=$(date "+%d")

#for loop that iterates over the city names assigned in the NAMES variable
for CITY in $NAMES;

	do 

#checks if the city name = gjovik 
if [[ "$CITY" == gjovik ]];

then 

#echoes Gjøvik, redirects it to file of that city name
	echo "Gjøvik" > $CITY.txt

#assigns variable API to the output of the data transfered by the curl command  
	API=$(curl -s "https://api.met.no/weatherapi/locationforecast/2.0/classic?lat=60.474466&lon=10.412958")
	
	

elif [ "$CITY" == bergen ];

then
	echo "Bergen" > $CITY.txt

	API=$(curl -s "https://api.met.no/weatherapi/locationforecast/2.0/classic?lat=60.233476&lon=5.192694")

elif [[ "$CITY" == oslo ]];

then
	echo "Oslo" > $CITY.txt

	API=$(curl -s "https://api.met.no/weatherapi/locationforecast/2.0/classic?lat=59.544583&lon=10.444592")

elif [[ "$CITY" == tromsoe ]];

then
	echo "Tromsø" > $CITY.txt

	API=$(curl -s "https://api.met.no/weatherapi/locationforecast/2.0/classic?lat=69.385604&lon=18.571829")

elif [[ "$CITY" == trondheim ]];

then
	echo "Trondheim" > $CITY.txt
 
	API=$(curl -s "https://api.met.no/weatherapi/locationforecast/2.0/classic?lat=63.254976&lon=10.234222")

elif [[ "$CITY" == kristiansand ]];

then
        echo "Kristiansand" > $CITY.txt
 
        API=$(curl -s "https://api.met.no/weatherapi/locationforecast/2.0/classic?lat=58.084816&lon=7.594416")


fi 

#For every loop/iteration the forecast data is scraped from the variable API, and appends data to said city's txt file 

	#temperature: greps string containing the word temperature
	#regualr expression matches any decimal number including negative numbers, decimal point, followed by one or more digits
	#use sed to remove the fractional part of the decimal numbers
	#fetches the first 4 instances of said regex and removes the first instance to get the current temperature + the temperature for the next two hours
	echo "$API" | grep -E "temperature" | grep -o -E "(-?[0-9]+\.[0-9]*)" | head -n4 | tail -n3 | sed -E "s/\..*//g" >> $CITY.txt

        #precipitation: greps the phrase code= and anything between two quotation marks
	#Fetches the third instance, greps only the perameters between the quotation marks, removes quotation marks
        echo "$API" | grep -E -o 'code=".*"' | head -n3 | tail -n1 | grep -E -o '".*"' | sed -E 's/^.//g; s/.$//g' >> $CITY.txt

	#date of scraping - variable assignment- appends to txt file 
	DATE=$(date "+%F %H:%M")

	echo $DATE >> $CITY.txt
	
	#humidity: similar operation to fetching percipitation, fetches the second instance to get the current humidity
	echo "$API" | grep -E "humidity" | grep -E -o 'value=".*"' | grep -E -o '".*"' | head -n2 | tail -n1 | sed -E 's/^.//g; s/.$//g; s/\..*//g' >> $CITY.txt	
done
	
	#creates directory named with formatted date assigned to DATE_FILE
	mkdir -p $DATE_FILE
	
	#moves all txt files to that directory
	mv *.txt $DATE_FILE
	
	#moves that directory to scraped_weather directory
	mv $DATE_FILE scraped_weather

#If statment which determines through the mod operator if the contants of variable DAY is an odd number.
#If the condition is true, then, the kristiansand.txt file will be deleted
#if the condition is false, nothing happens 
if (( $DAY % 2 == 1 ));
then 
	rm /home/andrchr/fetch/scraped_weather/$DATE_FILE/kristiansand.txt

else 
	:
fi



