#!/bin/bash


#Change dir to dir where scripts are stored so systemd can execute them.
cd /home/andrchr/fetch/
#scraped the data into txt files for all citys's + Kristisansand depending on odd or even.
./scraping_script.sh  
#generates the html pages for each city
./pages_script.sh
#generates the index/overview page with data used from the first scripts 
./overview_script.sh
#sends all the scraped weather data to ntfy.sh/idg1100-andrchr
./ntfy_script.sh
