#!/bin/bash

#causes script to exit imediatly if error occurs
set -eu

#overrides all other localization settings
LC_ALL=C

#assigning current date format to variable DATE
DATE=$(date "+%F-%H-%M")

#echo first half of html template and redirects it to index.html
 echo "<!DOCTYPE html>
	<html>
	<head>
        <meta charset="UTF-8">
        <title>Forecast - Overview</title>
        </head>
        <body>
        <h1>Weather Forecast</h1>
        <p>Forecast updated at: $DATE</p>
        <ul>" > index.html
#assign the value 1 to variable N
N=1

#loop through all txt files in current date dir in scraped_weather dir
for FILE in ./scraped_weather/$DATE/*.txt
do

	#fetch city name from first line in variable FILE using the head command, assigned to CITY_NAME
	CITY_NAME=$(cat $FILE | head -n1)

	#list all txt files in the current date directory, head -Nth line, tail last line, first line is assigned to varable FILE_NAME
	FILE_NAME=$(ls ./scraped_weather/$DATE/ | sed -E s/\.txt$//g | head -n$N | tail -n1)

	#For each loop/iteration N + 1 = next filename is assigned to the variable FILE_NAME
	N=$(($N + 1))

	#temperatures and precipitation is scraped from txt file using head/tail command
	TEMP_1=$(cat $FILE | head -n2 | tail -n1)

	TEMP_2=$(cat $FILE | head -n3 | tail -n1)

	TEMP_3=$(cat $FILE | head -n4 | tail -n1)

	PRE=$(cat $FILE | head -n5 | tail -n1)

	#echo an <li> element for each loop, the different forecast/date filename variables is placed and appended to index.html
      echo "<li><a href="cities/$DATE/$FILE_NAME.html">$CITY_NAME:</a> ($PRE) $TEMP_1 ℃, $TEMP_2  ℃, $TEMP_3 ℃</li><br>" >> index.html

done
	#rest of the html is added to index.html
      echo "</ul>
        </body>
        </html>" >> index.html

	

