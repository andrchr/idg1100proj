#!/bin/bash

set -eu
#Assigns variable DATE to the date-format file directory
DATE=$(date "+%F-%H-%M")

#Reads all txt files stored in $DATE and stores all the data in SCRAPED_NTFY
SCRAPED_NTFY=$(cat scraped_weather/$DATE/*.txt)

#Sends POST request to ntfy.sh containg the text stored in SCRAPED_NTFY
curl -d "CURRENT FORECAST IS: $SCRAPED_NTFY" ntfy.sh/idg1100-andrchr



